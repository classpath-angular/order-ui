import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { OrderComponent } from './order/order.component';
import {RouterModule} from '@angular/router';
import { CompOneComponent } from './comp-one/comp-one.component';
import { CompTwoComponent } from './comp-two/comp-two.component';
import {HttpClientModule} from '@angular/common/http';
import { SubmitOrderComponent } from './submit-order/submit-order.component';
import {FormsModule} from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    OrderComponent,
    CompOneComponent,
    CompTwoComponent,
    SubmitOrderComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path:'',
        component:CompOneComponent
      },
      {
        path:'one',
        component: CompOneComponent
      },
      {
        path:'two',
        component: CompTwoComponent
      },
      {
        path:'**',
        component: NotFoundComponent
      }
    ]),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
