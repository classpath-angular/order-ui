import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-comp-one',
  templateUrl: './comp-one.component.html',
  styleUrls: ['./comp-one.component.css']
})
export class CompOneComponent implements OnInit {

  orders:any[];

  selectedOrders:[];

  constructor(private orderService: OrderService){}

  ngOnInit(): void {
    this.orderService
      .getOrders()
      .subscribe(data => this.orders = data);
    this.selectedOrders = [];
  }
  select = function(order){
    //fetch the order with the orderId
    var index = this.orders.findIndex(o => order.id === o.id);
    console.log(`The index is ${index}`);
    if (index >= 0){
      this.orders.splice(index, 1);
      this.selectedOrders.push(order);
    }
  }  
  
  unselect = function(selectedOrder){
    //fetch the order with the orderId
    console.log('inside the unselect method..')
    console.log(selectedOrder)
    var index = this.selectedOrders.findIndex(o => selectedOrder.id === o.id);
    console.log(`The index is ${index}`);
    if (index >= 0){
      this.selectedOrders.splice(index, 1);
      this.orders.push(selectedOrder);
    }
  }
}
