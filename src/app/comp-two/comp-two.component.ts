import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comp-two',
  templateUrl: './comp-two.component.html',
  styleUrls: ['./comp-two.component.css']
})
export class CompTwoComponent implements OnInit {

  constructor(private orderService: OrderService, private route:Router){}

  ngOnInit(): void {
    
  }
  submitOrder = function(orderForm){
    var order = {order_date:'',merchant_name:'',line_items:[]  };
    order.order_date = '20-05-05';
    order.merchant_name = orderForm.merchantName;
    order.line_items.push({'name':orderForm.itemName, 'price': orderForm.itemPrice})
    this.orderService.placeOrder(order).subscribe(() => {
      this.route.navigate(['one']);
    });
  }
}
