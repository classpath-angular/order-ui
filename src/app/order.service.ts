import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient) {}


  getOrders ():Observable<any[]>{
    return this.httpClient.get<any>('http://localhost:8080/api/v1/orders');
  }

  placeOrder(order):Observable<any>{
    return this.httpClient.post<any>('http://localhost:8080/api/v1/orders', order )
  }
}
