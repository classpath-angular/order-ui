import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent {
  @Input('data') order;
  @Output('handleSelect') clicked = new EventEmitter();

  select = function($event){
    this.clicked.emit($event);
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('inside the on changes method of order component')
    console.log(this.order)
  }
}
